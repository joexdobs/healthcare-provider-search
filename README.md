### Healthcare Provider Search: ###

Provides a demonstration of service and GUI to search and locate healthcare providers using a combination full text, text prefix, value match and geo-filtered search.   *[Static Demo Screens](documentation/demo)*   [live demonstration](http://demo.bayesanalytic.com:9839/prov-search-basic.html)    Demonstrates a provider search micro-service written in node.js  using elastic search as it's back end.  It also demonstrates a RIA browser application that calls the service and renders large amounts of data fast.   If uses specialized elastic search indexes and geo-coding to make it easy to find doctors and specialists anyplace in the USA.  

![](documentation/demo/provider-search-basic.jpg)

**This demo uses public domain data downloaded from the US government.** 

[Full width view of this page](README.md)    [live demo](http://demo.bayesanalytic.com:9839/prov-search-basic.html)

The storage and search functionality is provided by elastic search with a middle-tier service providing a nice domain and simplified API provided by a node.js based HTTP server. 

- Provides RIA GUI using standard JavaScript and DHTML.
- The Search API is provided by a service written in Node.js.  The node JS service handles multiple queries against elastic search to deliver many API features.  It also delivers the data in a domain friendly form rather form and it serves up the HTML content and .js libraries as a normal HTTP server.
- The actual search and repository is provided by elastic search.
- Demonstrates a fast GeoPoint lookup from zip code or full address.   
- Can provide very TPS that is easily scalable in fault tolerant fashion by running inexpensive servers as a shared nothing bank of clones.

Some of this is coded specifically for this demonstrating but most of the underlying libraries are drawn from MIT licensed libraries and Utilities. 

### How do I get set up? ###

[Installation & Configuration on Centos Linux](centos-install-and-configuration.md)

[Installation & Configuration on Windows 10](windows-install-and-configuration.md)

- - ​

- #### Tests to Ensure Service Operation

     - Load:  http://127.0.0.1:9839/  It should display a index page with various test and demos.
     - Load: http://127.0.0.1:9839/status It should show a JSON structure showing various server config data. file showing whether the server can connect to required assets.
     - Load:  http://127.0.0.1:9200/prov/_stats  it should shown a statistics for the number of providers loaded provided the util/bulk-load.py utility ran successfully. 
     - Load: http://127.0.0.1:9200/prov/_mapping  It should show the default mappings for the fields created with with the exception of the mappings we specifically assigned such as geopoint for the loc field.
     - Load: [http://localhost:9200/prov/_search?q="s*"](http://localhost:9200/prov/_search?q="s*") Should display data that has at least one field that starts with "s"
     - Load: http://localhost:9200/location/loc/14094 It should display the geopoint record showing the centroid for zipcode 14094.
     - Load: http://127.0.0.1:9839/zipgeo/14094 it should display the lat, lon geopoint for that zip if the that zip code is present in the search index.  Http status code should be 200.
     - http://127.0.0.1:9839/zipgeo/1409499  It should return a http status code  404 document not found since it is not a valid zipcode.
     - http://127.0.0.1:9839/provsearch?zipcode=14094&maxDist=15  Should return a list of healthcare providers who are within 50 miles of the centroid for zipcode 14094.  Will return http status code 200 when successful or 404 if fails to find a match.  May return 300 series code when query parameters are not supplied.   zipcode is mandatory.  maxDist defaults to 10 miles when not supplied.   Max Distance is in miles. 
     - http://127.0.0.1:9839/provsearch?zipcode=14094&maxDist=15&v=specialty Runs the normal provider search but adds a aggregation describing all the specialty found in the result sets.  This is normally used to render a search context sensitive list of specialty.
     - http://127.0.0.1:9839/provsearch?zipcode=48876&maxDist=8&specialty=clinical%2520social%2520worker&v=specialty  Runs the search adding a specific specialty the the search filters.  This one should return about 4 records.

     #### API Documentation

     - See [api-docs.md](documentation/api-docs.md) Lists sample queries and shows how they are used. 

- ###### Configuring provSearch Server

     - In the file [config/config.ini](config/config.ini) the values for elastSearch, elastSearchHost, ElastSearchPort must be changed to reflect the install server and listen port for elastic search.

- #### How to Run Integration Tests

     - [Integration-tests.md](integration-test.md)

- #### Deployment instructions


- ##### Network & Deployment Topology

  - Physical Configuration

    - The Elastic search server must be installed on a machine with fast block storage.  Local SSD connected via ESATA or PCI storage is ideal.   Elastic search makes good use of Linux file cache so slower storage with lots of RAM can partially compensate during heavy read loads but reliably fast storage during heavy updates is critical.

    - The Node.js and elastic search server processes should run on the same virt to allow use of network bypass when exchanging data.  

    - Each cluster node in this configuration is ran as a shared nothing service where the only shared dependencies are the load balancer that routes traffic between the nodes and a Queue which feeds data to all nodes currently in the cluster.  Do not use the Elastic search Cluster features as this increases risk of total system failure while also dramatically reducing total TPS when compared to a shared nothing set of clones or toasters.    This does not always remain true for larger indexes where silos of servers work together as stand alone clusters. 

    - At least 3 copies of this stack should be ran on 3 separate virts which are running on on different physical cabinets should be used.  This allows 1 to be taken down for upgrade while still proving a depth of 1 for DRP failures.   Higher load systems may require more clones.

    - Note: Testing of this process was done on Windows 10.  It should run without change on Linux or Mac. 

    - Why run the Search process and Elastic Search on the same Virt

      - The elastic search process generates a lot of JSON much of which is filtered out to produce domain friendly JSON.  Having them on the same Virt allows this traffic to bypass most of the network looping back at high speed through the Linux network subsystem.
      - By keeping the traffic in the same virt we can bypass the need to encrypt the traffic between Elastic search and Node.js while still protecting the data.
      - In some instances we make multiple queries that are blocking or serial were the results of one query are used to adjust the 2nd query.  The in virt loopback is very fast which minimizes this.
      - There is a strong correlation between the capacity of a single lightweight node process which maxes out a 8 core elastic search instance so it makes for easy scaling to treat the 2 processes as a unit.

    - Why run the haproxy or nginx on same box. 
      The normal preferred mechanism would be to use a shared load balancer to route traffic between all nodes providing the same search service however in some instances this will not meet high performance in high security environments.

      When the standard load balancer can not be trusted to filter out attack traffic then we can use haproxy to provide an additional filter and prevent unintended network traffic from reaching node.js and elastic search.

      When there will be PII or PCI private data it is essential that this data be encrypted before leaving the service box or virt.   The current Node.js server does not support SSL and the node support for SSL is not the fastest available so installing HAProxy on the same virt allows intra-virt traffic to be unencrypted while HAProxy provides the SSL encryption between the virt / box and the shared load balancer.

  - The recommended network topology is to run each Node.js and elastic search server in a isolated network segment with a firewall that only allows access to it from identified servers. 

    - This can be enforced using Linux in-box firewall features so only processes running on the same linux virt are allowed to connect to the elastic search process.

  - A proxy or load balancer such as [haproxy](http://www.haproxy.org/) or [nginx](https://nginx.org/en/) should be used to grant access to the node.js http listener and only the proxy server should have network ability to reach the server via http.  
    - All other ports should be locked down to prevent access by any other system.
    - SSL termination is done in the proxy. 
      - No attempt should be made to add SSL to the connection between node.js and Elastic search as it consumes considerable CPU power and when configured as specified this traffic never actually transits the external network as it passes over the IP stack inside the Linux kernel to a process running on the same box.   The Elastic search process generates considerable volumes of JSON and we do not want to pay the cost of encrypting and decrypting it on the same box.
    - The Node.js process is not sufficiently robust to allow direct public exposure.  As such it must be protected behind a proxy such as HAProxy a a level 5 hardware proxy such as F5.
    - HAProxy should be configured to only allow specified requests through to the node.js service.
    - No server and no process other than the node.js or some operational monitoring should have network access to the elastic search process.
      - It is OK to allow temporary access for various developers to access both the node service and the elastic search service directly for diagnostics but this would be better done by allowing the users to login to the virt via SSH and run tests so they loop back via the linux kernel.
    - HAProxy is mentioned but there are are many equally good choices some of which are software and others are hardware.
- ​ During development we run the service and elastic search locally or on a small virt and access it directly without the HAProxy component.

### Contribution guidelines ###

* See:  [api-docs.md](documentation/api-docs.md)
* See:  [actions & Task planning](documentation/actions.md)
* See: [Example GUI used as design guide](examples/gui/readme.md)
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* [Joseph Ellsworth](http://BayesAnalytic.com/contact)  206-601-2985
* Please leave comments as issues on this repository.  

## Folder / Directory Structure

- [config](config) - stores a config.ini file used to tell the server where to look for it's elastic search files.  Can also be used to change log file destination, etc.

- [data](data) - Used to store local data but does not include data stored inside elastic search which manages it's own data directory

  - [import](data/import)  Used to store data we use to initialize the system including a original set of providers and geocoding data. This data is normally processed by various python utilities and bulk inserted into elastic search.

- [doc](doc) The document root of web server portion of the server.  Anything in this tree is available to be served by the http server.

  - [doc/js](doc/js) javascript files used by the GUI demo
  - [doc/css] CSS files used by the GUI to style the demo

- [documentation](documentation) Directory used to store various documentation and design files that did not fit nicely into other directories or to keep the code directories relatively clean.

  - [demo](documentation/demo) Directory used to show  screens and other demo so people can see what the system looks like before they install and run the services.
  - [examples](documentation/examples) Used  to contain data and screenshots used during the design collaboration process to document design intent. 
  - [examples/gui](documentation/examples/gui) Contains sample screenshots used as design guideline for GUI demo.

- [lib](lib) Contains various node.js library files used to provide the search and geocoding service.  Most of these are open source MIT but some of the handlers are specific to this application. 

- [log](log) Contains log files generated by the search process while operating.   The elastic search logs are managed independently.

- [util](util) Contains utility modules used to create, load and maintain the elastic search indexes. 

## License:

Most of this repository is Copyright (c) 2014 <Joseph Ellsworth, Bayes Analytic> - See use terms in [/License.txt](license.txt) in this repository.    

This system represents an example of provider search powered by Elastic search,  Node.js and our custom libraries and schema design.     ***If you have need of similar systems then please [contact us](http://BayesAnalytic.com/contact) for help***.   