##### Summary of set up on Windows

- [Download this repository](https://bitbucket.org/joexdobs/provider-locator/downloads/)
- [Download and unzip Elastic Search](https://www.elastic.co/downloads/elasticsearch).  Tested with version 5.4 but other versions will probably work.    We unzip ours in c:\jsoft\elastic\provider
- Download and install [Java](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) We are using version 1.98.0_131 supplied with the jdk8 downloads.
  - Add the Java and javac executable to local path.  We install our Java in /java/jdk
- Download and install [node.js](https://nodejs.org/en/download/) version V6.10.2 or newer 
  - Add the node executable to local path 
  - We install our node in /nodejs
  - See also: node-install-notes.txt
- Download and install [python version 2.7.12](https://www.python.org/downloads/) or newer. Python 3.0 series will not work.
  - Add the Python executable to the local path
  - We install our python in /python27

##### Starting the Server

- [Installation & Configuration on Centos Linux](centos-install-and-configuration.md)

- Starting Elastic search

  - Run the bin/elasticsearch.bat on windows or bin/elasticsearch on linux. This will start a default configuration of elastic search listening on port 9200.  This instance is normally protected behind a firewall so only the Node.js service and python utilities running on the same box can reach it with http requests.

  - ##### Starting Elastic Search

    cd to path where you unzipped elastic search
    Run bin/elasticsearch (or bin\elasticsearch.bat on Windows) 
    Run curl http://localhost:9200/
    Dive into the getting started guide and video


- ##### Starting the Node.js app server

  - Run the script start_server_9839.bat or equivalent .sh on linux.    Alternatively can run node directly using the command line: <u>node HTTPServer.js 9839 ./config ./data  > log/httpserver9839.log.txt</u>

- ##### Database configuration

  The following steps require that Elastic search is running on port 9200 and can be reached via http://127.0.0.1:9200

  - unzip [data/import/physicians.zip](data/import/physicians.zip) It should create a single file  data/import/physicians.tsv that  is used by the bulk_load_providers.py utility.
  - unzip [data/import/zipcodes-geo.zip](data/import/zipcodes-geo.zip)  It should create a single file data/import/zipcodes-geo.tsv that is used by the create_loc_index.py utility.
  - Run [util/create_loc_index.py](util/create_loc_index.py)  this is done using python 2.7 from command line.  It creates a index used to map zip-codes to lat/lon centroids and city / state names for each zipcode.   It also provides a index to store a cache of lat/lon points for specific addresses.
  - Run [util/create_prov_index.py](util/create_prov_index.py) this is done using python 2.7 from command line.  It creates the /provider/prov index with a custom mapping to support geopoint mapping and special analyzers used for various search features. This must be re-ran before bulk_load_providers.py when the mappings are changed.
  - Run [util/bulk_load_providers.py](util/bulk_load_providers.py) this is done using python 2.7 from command line.  loads providers from the supplied extract into Elastic search so they can be searched at high speed.   

  ​