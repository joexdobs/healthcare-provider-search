# This query searches physician combined name for ngrams
# to use as auto complete list filtered for those physicians
# closer than specified maxDistance.   Returns a aggregation
# of unique physician names.    
#
# When using the default "text" type fields it can not
# match on fractional words.   By using the nGram analyzer
# such as with names.edge it gives us the ability to 
# find fractional matches as we would expect.

# See: /license.txt in this repository   May-2017 by Joe Ellsworth

# Background:
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-geodistance-aggregation.html
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-terms-aggregation.html
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-filter-aggregation.html
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-significantterms-aggregation.html
#   https://www.elastic.co/guide/en/elasticsearch/guide/current/bool-query.html
#   

import json
import time
from httputil import *

query = """
{
  "size" : 0,
  "query": {
    "bool": {  
      "must" : {    
	      "match": { "combName.edge" : { "query" : "ja" }}
      },
      "filter": {
         "geo_distance": {
           "distance":  "5mi",
           "loc" : { "lat": 40.221703, "lon": -111.692438 }
         }
      }
    }
  },
  "aggs" : {
    "location" : {
       "terms" : { "field" : "combName.raw", "size" : 150 }
    }
  }
}
"""

aggs1 = """
  "aggs" : {
    "name" : {
       "terms" : { "field" : "combName.raw" }
    }
  }
"""


# This version would return a nested aggregation
# Showing which company each physician reports to.
aggs2 = """  "aggs" : {
    "location" : {
       "terms" : { "field" : "name.raw" },
	    "aggs" : {
           "name" : {
             "terms" : { "field" : "combName.raw" }
           }
		}
    }
  }
"""


  
start = time.time()    
uri = "/prov/prov/_search"
postStr = query
resStr = dohttp("POST", "127.0.0.1", 9200, uri, query)
elap = (time.time() - start) * 1000
print "resStr=", resStr  

tobj = json.loads(resStr)
tout = json.dumps(tobj, indent=2)

print "as formatted JSON=", tout
print "totTime=", elap, "ms" 
      
    
   

