# Example of Querying and grouping the resulting
# string into distance bands.
# See: /liscense.txt in this repository   May-2017 by Joe Ellsworth
#
# Background:
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-geodistance-aggregation.html
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-geodistance-aggregation.html


import json
import time
from httputil import *

# Produces a banded list distances with the number
# of Providers found in each distance band.  Since
# no query is specified it uses a match-all query
# as the primary input.  

query = """
{
  "size": 0,
  "aggs": {
    "rings": {
      "geo_distance": {
        "field": "loc",
        "origin": { "lat": 40.221703, "lon": -111.692438 },
		"unit" : "mi",
        "ranges": [
          {   "to": 2  },
          {   "from": 2, "to": 4  },
          {   "from": 4, "to": 6 },
          {   "from": 6, "to": 10 },
          {   "from": 10, "to": 15 },
          {   "from": 15, "to": 25 },
          {   "from": 25, "to": 50},
          {   "from": 50, "to": 100  },
          {   "from": 100, "to": 250 },
          {   "from": 250, "to": 500 }
        ]
      },
      "aggs": {
        "hits": {
          "top_hits": {
            "size": 5,
            "sort": {
              "uniqueLocKey": "asc"
            }
          }
        }
      }
    }
  }
}
"""

  
start = time.time()    
uri = "/prov/prov/_search"
postStr = query
resStr = dohttp("POST", "127.0.0.1", 9200, uri, query)
elap = (time.time() - start) * 1000
print "resStr=", resStr  

tobj = json.loads(resStr)
tout = json.dumps(tobj, indent=2)

print "totTime=", elap, "ms"
print "as formatted JSON=", tout
 
      
    
   

