# Creates a Elastic search index /prov with type of prov
# using a mapping that forces the loc field to be a geopoint
# so we can query it with filter by distance.

from httputil import *

# https://www.elastic.co/guide/en/elasticsearch/reference/current/multi-fields.html
# https://www.elastic.co/guide/en/elasticsearch/reference/current/search-suggesters-completion.html

# Elastic search does not automatically recognize
# geopoint type fields so we require the mapping hint
# to allow future search.

# Adding the fields.raw aspect for names makes them
# faster for aggregation grouping.

# Note:  The Copyto fields to allow aggregation and search of data
#  supplied in two fields.
#
#  The Edge N-Gram allows searching for partial matches on
#  tokens in the index that we will need to provide
#  autocomplete functionality.  The NGrams cust indexing
#  speed by about 30%.  When we set # of shards down to
#  1 it reducing indexing speed by about 50% from about
#  5.2K per second with shards down to about 3.6K per second
#  but it will improve accuracy of counts in facets. This
#  When running with 1 shard CPU consumption drops to about
#  52% so 2 shards are needed to keep 2 cores busy.

jsonstr = """{
 "settings": {
    "number_of_shards": 1,
    "analysis": {
      "analyzer": {
        "autocomplete": {
          "tokenizer": "autocomplete",
          "filter": [
            "lowercase"
          ]
        },
        "autocomplete_search": {
          "tokenizer": "lowercase"
        },
        "analyzer_keyword": {
          "tokenizer":"keyword",
          "filter":"lowercase"
        }                   
      },
      "tokenizer": {
        "autocomplete": {
          "type": "edge_ngram",
          "min_gram": 2,
          "max_gram": 10,
          "token_chars": [
            "letter"
          ]
        }
      }
    }
  },
  "mappings": {
    "prov": {
      "properties": {
        "loc": {
          "type": "geo_point"
        },
        "orgName": {
           "type": "text",
           "copy_to" : "names",
           "fielddata" : true,
           "store" : true,
           "fields": {
            "raw": { 
              "analyzer": "analyzer_keyword",
              "type":  "text",
              "fielddata" : true               
            }
          }
        },
        "combName" : {
          "type": "text",
          "copy_to" : "names",
          "fielddata" : true,
          "store": true,
          "fields": {
            "raw": { 
              "analyzer": "analyzer_keyword",
              "type":  "text",
              "fielddata" : true            
            },
            "edge" : {
              "type": "text",
              "analyzer": "autocomplete",
              "search_analyzer": "autocomplete_search"
            }
          }
        },
        "names" : {
          "type": "text",
          "copy_to" : "names",
          "fielddata" : true,
          "store": true,
          "fields": {
            "raw": {
              "analyzer": "analyzer_keyword",
              "type":  "text",
              "fielddata" : true
            },
            "edge" : {
              "type": "text",
              "analyzer": "autocomplete",
              "search_analyzer": "autocomplete_search"
            }
          }
        },        
        "uniqueLocKey": {
          "type": "string",
          "index" : "not_analyzed"          
        },
        "specialty" : {
               "type": "text",
               "fielddata" : true,
               "store": true,
               "fields": {
                  "raw": {
                     "analyzer": "analyzer_keyword",
                     "type":  "text",
                     "fielddata" : true
                  },
                  "edge" : {
                     "type": "text",
                     "analyzer": "autocomplete",
                     "search_analyzer": "autocomplete_search"
                  }
               }
        },
        "addr": {
          "properties" : {
            "city" : {
              "type" : "text",
              "fields": {
                "raw": { 
                  "type": "text",
                  "analyzer": "analyzer_keyword",                  
                  "fielddata" : true
                },
                "edge" : {
                   "type": "text",
                   "analyzer": "autocomplete",
                   "search_analyzer": "autocomplete_search"
                }
              }
            },
            "state" : {
              "type" : "keyword"
            },
            "zip" : {
              "type" : "keyword"
            }
          }
        }
      }
    }
  }
} """

dohttp("DELETE", "127.0.0.1", 9200, "/prov", "")
dohttp("PUT", "127.0.0.1", 9200, "/prov", jsonstr)

