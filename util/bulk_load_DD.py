# Utility to convert certificate of need data downloaded
# from FDA into JSON records to test form edit and
# retrieve functionality
# License:  Confidential Propriatary  5/26/2017

import json
import time
from httputil import *

def parseBool(avar):
  if avar == "Y":
    return True
  elif avar == "N":
    return False
  else:
    return None
  

def flushBuffer(abuff):
  print "flush len=", len(abuff)
  uri = "/prov/prov/_bulk"
  postStr = "\n".join(abuff)
  #print "uri=", uri, "postStr=", postStr
  resStr = dohttp("POST", "127.0.0.1", 9200, uri, postStr)
  # TODO: Add logic to parse the bulk insert results
  # to ensure that all items returned a 200 or add to
  # and error file.
  
  
start = time.time()    
f = open("../data/import/locator_extract.csv")
errFi = open("../data/import/locator_extract.err.txt", "w")
firstLine = f.readline().strip()
fldNames = firstLine.split(",")
print "fldNames=", fldNames
useNames = []
fldPos = {}
fldNdx = 0
for aname in fldNames:
  tname = aname.strip().replace(" ", "_").lower().replace("(","_").replace(")","").replace("__","_")
  useNames.append(tname)
  fldPos[tname] = fldNdx
  print "ndx=", fldNdx, tname
  fldNdx = fldNdx + 1
  
numFldHead = len(useNames)
print("colNames=", useNames)

errCnt = 0
outNames = []
recNum = -1
buffer = []
totRecAdd = 0
while True:
  recNum = recNum + 1
  aline = f.readline().strip()
  if aline <= "":
    break
  try:
    fvals = aline.split(",")    
    numFld = min(len(fvals), numFldHead)
    trec = {}
    if fvals[4] > " ":
      trec["medicadeId"] = fvals[4]

    trec["email"] = fvals[3]
    trec["fax"] = fvals[25]  
    trec["loc"] = {}
    trec["loc"]["lat"] = float(fvals[22])
    trec["loc"]["lon"] = float(fvals[21])
    if fvals[0] > " ":
      trec["gender"] = fvals[0]
    if fvals[1] > " ":
      trec["languages"] = fvals[1]
    else:
      trec["languages"] = None
      
    if fvals[2]> " ":
      trec["OfficeHours"] = fvals[2]

    trec["patientAgeRange"] = fvals[6]

    if fvals[7] > " ":
      trec["primaryLocation"] = parseBool(fvals[7])
    else:
      trec["primaryLocaiton"] = None
      
    
    trec["specialty"] = [ fvals[8]]
    if fvals[9] > " ":
      trec["specialty"].append(fvals[9])
    if fvals[10] > " ":
      trec["specialty"].append(fvals[10])
    cda = {}    
    trec["addr"] = cda
    cda["county"] = fvals[5]
    cda["street"] = fvals[11]
    cda["city"] = fvals[12]
    cda["state"] = fvals[13]
    cda["zip"] = fvals[14]
    cda["zipPlus4"] = fvals[15]
    trec["phone"] = fvals[16]

    stn = {}                   
    trec["drName"] = stn
    stn["last"] = fvals[17].strip()
    stn["middle"] = fvals[18].strip()
    stn["first"] = fvals[19].strip()
    trec["name"] = fvals[20].strip()
    trec["combName"] = stn["last"] + ", " + stn["first"] + " " + stn["middle"]
    trec["combName"] = trec["combName"].replace("  ", " ").strip()

    trec["exclude"] = parseBool(fvals[24])
    trec["npi"] = fvals[25]

    treats = {}
    if fvals[28] > " ":
      treats["disabledChildren"] = fvals[28]
    else:
      treats["disabledChildren"] = None
      
    if fvals[29] > " ":
      treats["disabledAdults"] = fvals[29]
    else:
      treats["disabledAdults"] = None
    
  
    
    if fvals[30] > " ":
      trec["publicTransitAccess"] = parseBool(fvals[30])
    else:
      trec["publicTransitAccess"] = None
      
    if fvals[32] > " ":
      trec["handicapAccess"] = parseBool(fvals[32])
    else:
      trec["handicapAccess"] = None
      
      
    trec["product"] =fvals[33]

    if fvals[31] > " ":
      trec["medicareOptOut"] = parseBool(fvals[31])
      
    trec["uniqueLocKey"] = trec["name"] + ".." + trec["addr"]["street"] + ".." + trec["addr"]["zip"]

    aStr = json.dumps(trec)
    #print recNum, " asJSON=", aStr

    buffer.append(json.dumps({ "index" : { }}))
    buffer.append(aStr)
    totRecAdd += 1
    if len(buffer) > 15000:
          print "buffLen=", len(buffer), "totRecAdd=", totRecAdd 
          flushBuffer(buffer)
          del buffer[:]
          curr = time.time()
          elap = curr - start
          print "totTime=", elap,  "totRecAdd=", totRecAdd, " recsPerSec=", totRecAdd / elap, " errCnt=" + str(errCnt)
          
          
          

          
  except ValueError:
    errCnt += 1
    errFi.write("errCnt=" + str(errCnt) + " parse error aLine=" +  aline + "\tfvals=" + str(fvals) + "\n")
  
errFi.close()
curr = time.time()
elap = curr - start
print "totTime=", elap,  "totRecAdd=", totRecAdd, " recsPerSec=", totRecAdd / elap, " errCnt=", str(errCnt)
 
      
    
   

