# License:  See:  /license.txt in this repository

import httplib
import json

def printText(txt):
    lines = txt.split('\n')
    for line in lines:
        print line.strip()



# Elastic search does not automatically recognize
# geopoint type fields so we require the mapping hint
# to allow future search.

def dohttp(verb, server, port, uri, postStr):
    httpServ = httplib.HTTPConnection(server, port)
    httpServ.connect()

    httpServ.request(verb, uri, postStr)
    response = httpServ.getresponse()
    respTxt = response.read()
    if response.status == httplib.OK:
        print "xcmd sucess"
    else:
        print "cmd fail"
        print "server", server, " port=", port, " uri=", uri
        print "postStr=", postStr
        print "Output from PUT request"
        printText (respTxt)
    httpServ.close()
    return respTxt

