# This query searches City nGram within a given distance
# of the specified geopoint and returns the list of cities
# that contain words that start with the specified 
# search text. 
#
# When using the default "text" type fields it can not
# match on fractional words.   By using the nGram analyzer
# such as with names.edge it gives us the ability to 
# find fractional matches as we would expect.

# See: /license.txt in this repository   May-2017 by Joe Ellsworth

# Background:
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-geodistance-aggregation.html
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-terms-aggregation.html
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-filter-aggregation.html
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-significantterms-aggregation.html
#   https://www.elastic.co/guide/en/elasticsearch/guide/current/bool-query.html
#   


# Examples querying for the edge nGram semanitic
# http://127.0.0.1:9200/location/loc/_search?q=city.edge:AIBON
#http://127.0.0.1:9200/location/loc/_search?q=city.edge:fran

import json
import time
from httputil import *

a = """"bool": {  
      "must" : {    
	      "match": { "city" : { "query" : "AIBONITO" }}
      }
    }"""

query =  {
  "size" : 0,
  "query": {
    "bool" : {
      "must" : [
        {"match" : { "city.edge" : "fran" }},
        {"term" : { "state" : "CA"}}
      ]
    }
  }
}


aggs = {  
    "state" : {
       "terms" : {
          "field" : "state",
          "order" : { "_term" : "asc" }
       },
       "aggs" : {
         "city" : {
           "terms" : {
               "field" : "city.raw",
               "order" : { "_term" : "asc" }
           },
           "aggs" : {
             "zip" : {
               "terms" : {
                 "field" : "zip",
                 "order" : { "_term" : "asc" }
               }
             }
           }
         }
       }
    }
  }


query["aggs"] = aggs

postStr = json.dumps(query, indent=2)
print "postStr=", postStr
  
start = time.time()    
uri = "/location/loc/_search"
resStr = dohttp("POST", "127.0.0.1", 9200, uri, postStr)
elap = (time.time() - start) * 1000
print "resStr=", resStr  

tobj = json.loads(resStr)
tout = json.dumps(tobj, indent=2)



print "as formatted JSON=", tout
print "totTime=", elap, "ms" 
      
    
   

