# Creates a Elastic search index /location with type of loc
# using a mapping that forces the loc field to be a geopoint
# so we can query it with filter by distance.
# 
# This index is also used as a cache of Google calls to find the
# geo point for a given address.   We save both the full address
# and zip codes when we look them up in google to allow more
# precise filters.

# License:  See:  /license.txt in this repository


from httputil import *

def createLocationIndex():


 # Adding the edge_ngram allows better use
 # for auto complete features.  Adding the
 # fielddata: true allows the system to 
 
  CreatIndexWithMapJson = """{ 
  "settings": {
    "number_of_shards": 1,
    "analysis": {
      "analyzer": {
        "autocomplete": {
          "tokenizer": "autocomplete",
          "filter": [
            "lowercase"
          ]
        },
        "autocomplete_search": {
          "tokenizer": "lowercase"
        },
        "analyzer_keyword": {
          "tokenizer":"keyword",
          "filter":"lowercase"
        }                   
      },
      "tokenizer": {
        "autocomplete": {
          "type": "edge_ngram",
          "min_gram": 2,
          "max_gram": 10,
          "token_chars": [
            "letter"
          ]
        }
      }
    }
  },
  "mappings": {
      "loc": {
        "properties": {
          "loc": {
            "type": "geo_point"
          },
          "zip" : {
            "type" : "keyword",            
            "store": true
          },
          "state" : {
            "type" : "keyword",            
            "store": true
          },
          "city" : {
            "type" : "text",
            "fielddata" : true,
            "store": true,
            "fields": {
              "raw": { 
                "analyzer": "analyzer_keyword",
                "type":  "text",
                "fielddata" : true            
              },
              "edge" : {
                "type": "text",
                "analyzer": "autocomplete",
                "search_analyzer": "autocomplete_search"
              }
            }
          }          
        }
      }
    }
  } """
  dohttp("DELETE", "127.0.0.1", 9200, "/location", "")
  dohttp("PUT", "127.0.0.1", 9200, "/location", CreatIndexWithMapJson)


  # Adding some sample data
  # we will add to this as new queries come in
  # to minimize the number of times we have to
  # call the Google API


#### Load the Rest of Lat Lon by Zipcode
## Deonstrates adding records  in bulk posts of 500
## adt a time. 1 at a time to the
## elastic search index.  Normally would use bulk
## index commands when adding more than a few
## records
def flushBuffer(abuff):
  print "flush len=", len(abuff)
  uri = "/location/loc/_bulk"
  postStr = "\n".join(abuff)
  #print "uri=", uri, "postStr=", postStr
  dohttp("POST", "127.0.0.1", 9200, uri, postStr)
  # TODO: Add logic to parse the bulk insert results
  # to ensure that all items returned a 200 or add to
  # and error file.

FPZip = 0
FPCity = 2
FPState = 3
FPLat = 5
FPLon = 6


def loadZips():
  fi = open("../data/import/zipcodes-geo.tsv")
  head = fi.readline()
  buffer = []
  totRecAdd = 0
  while True:
    aline = fi.readline().strip()
    if aline <= "":
      break
    try:
      fvals = aline.split("\t")
      zipcode = "%05d" % int(fvals[FPZip])
      city = fvals[FPCity]
      state = fvals[FPState]
      lat = float(fvals[FPLat])
      lon = float(fvals[FPLon])
      buffer.append(json.dumps({ "index" : {"_id" : zipcode }}))
      trec = { "zip" : zipcode, "loc" : { "lat" : lat, "lon" : lon }, "city" : city, "state" : state }
      totRecAdd = totRecAdd + 1
      #print "tra=", totRecAdd,  " len(buffer)=", len(buffer)
      buffer.append(json.dumps(trec))
      
      if len(buffer) > 5000:
          print "buffLen=", len(buffer), "totRecAdd=", totRecAdd 
          flushBuffer(buffer)
          del buffer[:]
          buffLen = 0;
      
    
    except ValueError:
      print "parse error", aline, " fvals=", fvals
      
  if len(buffer) > 0:
    flushBuffer(buffer)

##########
## MAIN
##########
createLocationIndex()
loadZips()
