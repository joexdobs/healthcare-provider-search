# Example of Querying on a text filter and
# locating matching names to use for auto
# suggest.
# See: /liscense.txt in this repository   May-2017 by Joe Ellsworth
# Background:
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-geodistance-aggregation.html
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-terms-aggregation.html
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-filter-aggregation.html
#   https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-significantterms-aggregation.html
#   https://www.elastic.co/guide/en/elasticsearch/guide/current/bool-query.html
#   

import json
import time
from httputil import *

# This query searches both the name (business name) and
# combName (doctors) name field for the tokens specified
# in this instance "thomas"  It returns two aggregations the
# list of unique business names where it found the tokens
# and the list of unique doctor names where it found the
# token.  This is designed to allow a auto suggest field
# which is populated based what the user types so if they
# type "john" we could show two lists those where there are
# doctors that contain john and a second where we show
# the list of facilities where a "john" works. 
#
# When using the default "text" type fields it can not
# match on fractional words.   By using the nGram analyzer
# such as with names.edge it gives us the ability to 
# find fractional matches as we would expect.

query = """
{
  "size" : 0,
  "query": {
    "bool": {
      "should": [
	     { "match": { "names.edge" : { "query" : "THOM" }}},
         { "match": { "orgName" : { "query" : "THOM", "operator" : "and" }}},
         { "match": { "combName" : { "query" : "THOM", "operator" : "and" }}}
      ],
      "minimum_should_match" : 1,
      "filter": {
         "geo_distance": {
           "distance":  "250mi",
           "loc" : { "lat": 40.221703, "lon": -111.692438 }
         }
      }
    }
  },
  "aggs" : {
    "busnames" : {
       "terms" : { "field" : "orgName.raw" },
       "aggs" : {
         "drnames" : {
           "terms" : { "field" : "combName.raw" }
         }
       }
    },
    "drnames" : {
       "terms" : { "field" : "combName.raw" }
    },
    "names" : {
       "terms" : { "field" : "names.raw" }
    },
    "specialty" : {
       "terms" : { "field" : "specialty" }
    }
  }
}
"""



  
start = time.time()    
uri = "/prov/prov/_search"
postStr = query
resStr = dohttp("POST", "127.0.0.1", 9200, uri, query)
elap = (time.time() - start) * 1000
print "resStr=", resStr  

tobj = json.loads(resStr)
tout = json.dumps(tobj, indent=2)



print "as formatted JSON=", tout
print "totTime=", elap, "ms" 
      
    
   

