### Centos Install & Configuration of Provider Search 

```
# jwork is a example user ID you intend to use to run the service
# under.  It should be replaced with the desired user ID.

# Create the service account user
useradd jwork 
  creates the user jwork

# Set the password for the service account user
passwd jwork
  enter desired password for jwork

# Add user to list of users allowed to run sudo commands.
# in some cases this can be removed once the service is fully
# installed since sudo is not needed to run once everything
# is installed.
usermod -aG wheel jwork
  # Add user to group capable of sudo
  #User must log out and re-login before new group will be active.

Login as jwork


# Install the git client so we can pull source from 
# repository.
sudo yum -y install git

# Install unzip so we can decompress files in a future step.
sudo yum -y install unzip

# Install wget which seems to work more reliably when downloading binary
# files than curl. 
sudo yum -y install wget
  

# Note: the Java 1.7 open SDK is not adequate must have Java 1.8 or latter. sudo yum -y install java-1.7.0-openjdk

# Download J2SE JRE or JDK.  Only the JRE is needed if your service
# will  be fully implemented in node.js like this one.  You must accept
# license agreement so I downloaded using a browser and then SCP the file
# to the server. 
  http://www.oracle.com/technetwork/java/javase/downloads/index.html 
  http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.htm


sudo yum localinstall jre-9.0.1_linux-x64_bin.rpm
# Modify rpm name to match what was downloaded from Oracle

# Start Elastic Search so it will continue running after disconnect.
nohup elasticsearch &

# Install Node.js
wget http://nodejs.org/dist/latest/node-v9.0.0-linux-x64.tar.gz
# May need to get the most recent file name from http://nodejs.org/dist/latest/
tar xzvf node-v* && cd node-v*

# Moves the node runtime to where it is easily re-used by 
# multiple services.
 sudo mv node-v9.0.0-linux-x64 /usr/bin/node
 
 #Edit .bash_profile so path includes #/usr/bin/node/bin
 #  I used vi to edit ~/.bash_profile but this could could be
 #  scripted
 
 # Downloads the service code.
 git clone https://joexdobs@bitbucket.org/joexdobs/healthcare-provider-search.git
  
 cd healthcare-provider-search/
 
 # Expands the sample data in the zip files 
 # because we saved it in zip form to save space 
 # on bitbuckets.
 cd ../data/import
 unzip physicians.zip
 unzip zipcodes-geo.zip
 cd ../../util
 
 # Creates the indexes in Elastic search and loads them with the 
 # provider detail.   This will fail if you forget to unzip the
 # files in the previous step.
 
 python create_loc_index.py
 python create_prov_index.py
 python bulk_load_DD.py
 python bulk_load_providers.py

# This is the time it took on my sample server to load the index
# totTime= 983.53016901 totRecAdd= 2280304  recsPerSec= 2318.48912402  errCnt=0
# totTime= 983.89805603 totRecAdd= 2286469  recsPerSec= 2323.88811624  errCnt= 0
cd ..

# Starts the servic server so it will continue running
# after the the ssh session is closed. 
nohup node HTTPServer.js 9839 ./config ./data  > log/httpserver9839.log.txt &

# Open the following URI:
http://104.167.2.158:9839/prov-search-basic.html
http://demo.bayesanalytic.com:9839/prov-search-basic.html

See Also: https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html

# TODO: Modify linux config to disallow all external connections except SSH and the specific service ports we want to open.
```

