// prov-search-basic.js
// view, domain and controller for simple 2 way bound form



/* ##########
 * ####  Provider Search region
 * ########## */



function showDetail(aProvId) {
    //alert("showDetail id=" + aProvId);
    var divid = "locdet_" + aProvId;
    var b = new String_builder;
   
    if ( aProvId in GContext.provNdx) {
        
        var aprov = GContext.provNdx[aProvId];
        var divid = "locdet_" + aProvId;
        showDiv(divid);  
        b.start("div", { "class": "prov_details", "onclick": "hideDetail(" + aProvId + ")" });

        function genfield(label, data) {
            b.start("div", { "class": "pdetgroup" });
            b.make("div", { "class": "pdetlabel" }, label);
            b.make("div", { "class": "pdetdata" }, data);
            b.finish("div");
        };

        genfield("NPI", aprov.npi);
        genfield("Medicade ID", aprov.medicadeId);
        genfield("phone", aprov.phone);
        genfield("medSchool", aprov.medSchool);
        genfield("gradYear", aprov.gradYear);
        if ("locations" in aprov) {
            var locs = aprov.locations;
            for (var locndx in locs) {
                var ahosp = locs[locndx];
                genfield("hospital", ahosp.lbn + "--" + ahosp.ccn);
            }
        }

        b.finish("div");
        b.toDiv(divid,  "prov_details");
    } else {
        alert(" no provider with id=" + aProvId + " can be found");
    };
}


function hideDetail(aProvId) {
        var divid = "locdet_" + aProvId;
        hideDiv(divid);
}


/*
    <select id="frmSpecialty" onchange="filterChange(this, GContext, 'specialty')" onkeyup="filterChange(this, GContext, 'specialty')" width="20"
                    title="The selected specialty is used to filter the list of providers shown to inclue only those that list this specialty">
              <option selected value="ANY">ANY</option>
              <option value="000">General Dentist</option>
            </select>
*/
/* Render select list with a specified set of options */
function renderSpecialtySelect(selvalues) {
    var b = new String_builder;
    b.start("select", {
        "id": "frmSpecialty",
        "onchange": "filterChange(this, GContext, 'specialty')",
        "onkeyup": "filterChange(this, GContext, 'specialty')",
        "width": "20",
        "title": "The selected specialty is used to filter the list of providers shown to inclue only those that list this specialty"
    });
    b.make("option", { "value": "ANY" }, ANY);
    for (var ndx in selvalues) {
        b.make("option", { "value": topt }, topt);
    }
    b.finish("select");
    b.toDiv("frmValueSpecialty");
}

/* Change a set of options for a given select list based
on the set of supplied options */
function setOptionsForSelect(optList, divId) {
    var adiv = document.getElementById(divId);
    var opts = adiv.options;
    var startoptlen = opts.length;
    var selndx = adiv.selectedIndex;
    var selopt = opts[selndx];
    var selval = selopt.value;
    var newSelNdx = -1;
    var opt = null;
    // If too many options on current list must
    // remove the extras.
    while (opts.length > optList.length) {
        opts.remove(opts.length - 1);
    } 
    // change the text and value for existing options
    // to match the new list or create new elements
    // as needed to extend the list. 
    for (var ondx in optList) {
        var aspec = optList[ondx];
        if (ondx >= startoptlen) {
            // Must create new options if our list has changed.
            opt = document.createElement('option');
            opts.add(opt, null);
        } else {
            opt = opts[ondx];
        }
        if ((aspec.name !== "ANY") && ("count" in aspec)) {
          opt.text = aspec.name.substring(0, 24) + " (" + aspec.count + ")";
        } else {
            opt.text = aspec.name;
        }
        
        opt.value = aspec.name;
        if (opt.value === selval) {
            opt.selected = true;
            newSelNdx = ondx;
        }
    }
    //if (newSelNdx !== -1) {
    //    adiv.selectedIndex = "" + newSelNdx;
    //}
    
    // TODO: Restore user selecteed option if 
    // still present in the list.
    //document.getElementById("mySelect").selectedIndex = "2";
    //document.getElementById("orange").selected = true;
    //var e = document.getElementById("ddlViewBy");
    //var strUser = e.options[e.selectedIndex].text;
}

function renderProvSearchResults(pobj, context) {
    context.provNdx = {};
    var provNdx = context.provNdx;
    var b = new String_builder;
    b.start("table", { "class": "prov_results"  });
    var locations = pobj.locations;
    var tin = pobj.in;
    if (tin.loc !== undefined) {
      var loc = tin.loc;
      toDiv("fldDispGeopoint", "lat: "  + comma_formated(loc.lat,3) + " lon: " + comma_formated(loc.lon,3));
    }
    var rowColr = "EA";
    b.start("tr");
    b.make("th", { "class": "tbldAddr" }, "Location");
    b.make("th", { "class": "tbldDoct" }, "Doctors");
    //b.make("th", { "class": "tbldSpec" }, "Specialty");
    b.make("th", { "class": "tbldDist" }, "Distance");
    b.finish("tr");
    for (var locndx in locations) {
        var aloc = locations[locndx];
        b.start("tr");
        b.start("td", { "class": "tbldAddr" });
        b.make("div", { "class" : "locName"}, aloc.orgName);
        b.make("div", { "class": "locstreet" }, aloc.addr.street);
        b.make("div", { "class": "loccity" }, aloc.addr.city);
        b.make("div", { "class": "locstate" }, aloc.addr.state);
        b.make("div", { "class": "loczip" }, aloc.addr.zip);
        b.make("div", { "class": "locphone" }, formatPhone(aloc.phone));
        b.finish("td");

        var providers = aloc.providers;
        b.start("td", { "class": "tbldDoct" });         
        var alreadyRendered = {};
        for (provndx in providers) {
            var aprov = providers[provndx];
            provNdx[aprov._id] = aprov;
            var combName = aprov.drName.first + " " + aprov.drName.middle + " " + aprov.drName.last;
            if (combName in alreadyRendered) {

            }
            else {
                b.start("div", { "class": "locdrNameSpec" });
                b.make("div", { "class": "locdrname", "onclick" : "showDetail(" + aprov._id + ")" }, combName);
                alreadyRendered[combName] = true;
                b.make("div", { "class": "locdrspec" }, aprov.specialty.join(", "));
                b.make("div", { "class": "locdrdetail", "id": "locdet_" + aprov._id, "onclick" : "hideDetail("  + aprov._id + ")"  });
                b.finish("div");
            }            
        }
        b.finish("td");
        //b.make("td", { "class": "tblSpec" }, "general+-1");
        b.make("td", { "class": "tblDist" }, comma_formated(aloc.distance,1));

        b.finish("tr");
       
    }

    b.finish("table");
    b.toDiv("search_res");
}



// Process message header data received from server
function runProvSearchOnData(err, pobj, parms) {
    context = parms.context;
    if (err) {
        console.log("runProvSearchOnData() err=" + err);
        toDiv("ErrorMsg", "Failure getCertOfNeed\n" + err);
        toDiv("search_res", "<h2 class='noProviderErr'>No Providers Found with Current Criteria</h2>");
        toDiv("fldDispGeopoint", "error");
    } else {
        //console.log("runProvSearchOnData() pobj=" + JSON.stringify(pobj, null, 2));        
        context.isDirty = false;
        renderProvSearchResults(pobj, context);
        if ("specialty" in pobj) {
            var spec = pobj.specialty;
            spec.unshift({ "name": "ANY", "count": 0 });
            setOptionsForSelect(spec, "frmSpecialty");
        }
        context.lastLoad = curr_time();
    }
}



// Fetch Certificate of Need for a given Cert 
// http://127.0.0.1:9839/provsearch?zipcode=14094&maxDist=50&v=specialty
function runProvSearch(context) {
    if ((context.zip.length !== 5) && (context.zip.length !== 10)) {
        // Can not run if we know we will not find a valid zipcode
        return;
    }

    toDiv("fldDispGeopoint", "working");
    var parms = {};
    parms.context = context
    context.startLoad = curr_time();
    qparms = { "zipcode": context.zip, "maxDist" : context.maxDist };

    if (context.street > "") {
        qparms.street = context.street;
    }

    if (context.name > "") {
        qparms.name = context.name;
    }

    if (context.state != "ANY") {
        qparms.state = context.state;
    }

    if (context.specialty != "ANY") {
        qparms.specialty = context.specialty;
    }

    // Add the request for specialty list data
    // to the service call.
    qparms.v = "specialty";


    parms.req_uri = makeURIWithQueryString("/provsearch", qparms)

    console.log("runSearch uri = " + parms.req_uri);
    parms.req_headers = {
        'Content-Type': "application/json",
        'Authorization': GContext.accessToken
    };
    parms.req_method = "GET";
    parms.callback = runProvSearchOnData;        
    makeJSONCall(parms, runProvSearchOnData);
}



/* ##########
 * ####  CitySearch region
 * ########## */

function setZip(zip) {
    var tdiv = document.getElementById("frmZip");
    setFormValue("frmZip", zip);
    filterChange(tdiv, GContext, "zip");
}

function renderCitySearchResults(pobj, context) {
    var b = new String_builder;
    b.start("div", { "class": "city_search",  "id" : "city_search" });
    var states = pobj.states;
    var tin = pobj.in;
    if (pobj.hits.count < 1) {
        toDiv("fldDispGeopoint", "no cities");
        toDiv("search_res", "<h1>No Cities found with current criteria</h1>");
        return;
    }
    var rowColr = "EA";
    for (var statendx in states) {
        var state = states[statendx];
        b.start("div", { "class": "state" });
        b.make("div", { "class": "statename" }, state.code);
               
        var cities = state.cities;
        for (var cityndx in cities) {
            var city = cities[cityndx];
            b.start("div", { "class": "city" })
            b.make("div", { "class": "cityname" }, city.name);
            var zips = city.zip
            b.start("div", { "class": "zips" });
            for (zipndx in zips) {
                var azip = zips[zipndx];
                b.make("div", { "class": "zip", "id": "zip_" + azip, "zip" : azip, "onClick" : 'setZip(' + azip + ')' }, azip);
            }
            b.finish("div"); // zips
            b.finish("div"); // city
        }
        b.finish("div"); // state
    }
    b.finish("div");
    b.toDiv("search_res");
}


// Process message header data received from server
function runCitySearchOnData(err, pobj, parms) {
    context = parms.context;
    if (err) {
        console.log("runCitySearchOnData() err=" + err);
        toDiv("ErrorMsg", "Failure CitySearech\n" + err);
        toDiv("search_res", "<h2 class='noSearch'>No Cities found with Current Criteria</h2>");
        toDiv("fldDispGeopoint", "error");
    } else {
        //console.log("runProvSearchOnData() pobj=" + JSON.stringify(pobj, null, 2));        
        context.isDirty = false;
        renderCitySearchResults(pobj, context);
        context.lastLoad = curr_time();
    }
}

// Fetch Certificate of Need for a given Cert 
// http://127.0.0.1:9839/provsearch?zipcode=14094&maxDist=50 
function runCitySearch(context) {
    if ((context.zip.length !== 5) && (context.zip.length !== 10)) {
        // Can not run if we know we will not find a valid zipcode
        return;
    }
    toDiv("fldDispGeopoint", "working");
    var parms = {};
    parms.context = context
    context.startLoad = curr_time();
    qparms = {  };

    if (context.city >= "") {
        qparms.city = context.city;
    }

    if (context.state != "ANY") {
        qparms.state = context.state;
    }
    
    parms.req_uri = makeURIWithQueryString("/citysearch", qparms)

    console.log("runSearch uri = " + parms.req_uri);
    parms.req_headers = {
        'Content-Type': "application/json",
        'Authorization': GContext.accessToken
    };
    parms.req_method = "GET";
    parms.callback = runCitySearchOnData;
    makeJSONCall(parms, parms.callback );
}


/* ##########
 * ####  Filter Change  domain
 * ########## */

function filterChange(field, context, fldName) {
    var fldVal = field.value;
    if (fldName === "zip") {
        if (context.zip !== fldVal) {
            context.zip = fldVal;
            runProvSearch(context);
        }
    } else if (fldName == "maxDist") {
        if (fldVal !== context.maxDist) {
            context.maxDist = fldVal;
            runProvSearch(context);
        }
    } else if (fldName == "name") {
        if (fldVal !== context.name) {
            context.name = fldVal;
            if (fldVal.length >= 2) {
                runProvSearch(context);
            }
        }
    }
    else if (fldName == "specialty") {
        fldVal = field[field.selectedIndex].value;
        //if (fldVal !== context.specialty) {
            context.specialty = fldVal;
            runProvSearch(context);
        //}
    } else if (fldName == "street") {
        if (fldVal !== context.street) {
            context.street = fldVal;
            runFindAddr(context);
        }
    }
    else if (fldName == "city") {
        if (fldVal !== context.city) {
            context.city = fldVal;
            runCitySearch(context);
        }
    }
    else if (fldName == "state") {
        if (fldVal !== context.street) {
            context.state = fldVal;  
            runCitySearch(context);
        }
    }
}



function clearSearch(context) {
    
}

