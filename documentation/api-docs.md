# Sample Queries for Service



## Utility And Health Check


- GET  http://127.0.0.1:9839/  It should display a index page with various test and demos.     This demonstrates the server is running and able to serve static content.
- GET http://127.0.0.1:9839/status It should show a JSON structure showing various server config data. file showing whether the server can connect to required assets.   *TODO: Add Link to result file*.
- ​


## GeoPoint Mapping

* GET http://127.0.0.1:9839/zipgeo/14094 it should display the lat, lon geopoint for that zip if the that zip code is present in the search index.  Http status code should be 200.   *TODO: Add link to result*

* GET  http://127.0.0.1:9839/zipgeo/1409499  GeoPoint lookup for Zipcode.  It should return a http status code  404 document not found since it is not a valid zipcode.  *TODO: Add link to result*

* GET http://127.0.0.1:9839/zipgeo/1409499  It should return a http status code  404 document not found since it is not a valid zipcode.  *TODO: Add link to result*

  ​

## Provider Search

* ###### TODO: Fill these in with sample queries we can use to test the capability of the provSearch Service

* Basic Query - Searches by Zipcode to find a Geopoint centroid for that Zipcode then loads a list of Providers based on the specified GeoFilter. 

  * GET http://127.0.0.1:9839/provsearch?zipcode=14094&maxDist=50  Should return a list of healthcare providers who are within 50 miles of the centroid for zipcode 14094.  Will return http status code 200 when successful or 404 if fails to find a match.  May return 300 series code when query parameters are not supplied.   zipcode is mandatory.  maxDist defaults to 10 miles when not supplied.   Max Distance is in miles.   *TODO: Add link to result*

* TODO: - First Name Filter - Expands basic search to include search for provider names that match some or all of specified search string.

* TODO: - Business Name filter - Expands Basic search to include search for company name.

* TODO: - Specialty Filter - Expands other basic searches to filter for providers that service the specified specialty.

* TODO:  - Search by State to find a list of Zipcodes

* TODO:  - Search by State to find Zip Centroid then find Dr within Geo Distance.




## City Search

* http://127.0.0.1:9839/citysearch?city=fran  Search for all cities that have one word starting with the word fran.

* http://127.0.0.1:9839/citysearch?city=fran&state=CA Search for all cities in California with a word starting with "fran".

* http://127.0.0.1:9839/citysearch?city=fran&maxDist=250&lat=38.576&lon=-121.48  Search for all cities in California within 250 miles of the specified lat,lon.

* http://127.0.0.1:9839/citysearch?maxDist=250&lat=38.576&lon=-121.48   search for all cities within 100 miles of the specified geopoint.

* http://127.0.0.1:9839/citysearch?city=franzx&maxDist=250&lat=38.576&lon=-121.48  Should return a hits.count = 0 and a empty states array because the system could not find a city starting with franzx within 250 miles of the specified geopoint.

* http://127.0.0.1:9839/citysearch?city=sea&state=ca Search for all cities in CA (California) which have a word starting with "sea".

  ​

