# TODO & Action Planning  



## Setup

* Obtain list of states used to populate State dropdown
* Obtain list of text for Specialty list population.   Also need mapping of Specialty code to Specialty Text.



## Demo GUI

* Specialty display should populate from list of specialties from providers currently in the filtered search result set

* Specialty filter should be applied when user has selected in the GUI.

* Provider Name should be applied as a filter when user has entered text.

* Providers should be returned sorted by distance to centroid. 

* When running a new search the existing search results should change color to indicate invalidated. 

* When rendering a new search need to force scrollbar of parent to top.

* DONE: 2017-05-29: Display Geopoint for selected search.

* --DONE: 2017-05-29: When zipcode lookup fails should show Zipcode not found message on the right.

* --DONE: 2017-05-29: When no providers are found or error from provider search should show no providers located on the right.

* Add a clear to reset form to initial state.

* Add a zipcode, max distance to URI in # tags. Parse on page load to allow external pages to control what is initially displayed.    Update these as the user makes selection so the link can be emailed.

* Allow system to use client side storage to record search configuration settings so they restore the next time the page is loaded.   This should be optional and default to off.

* Add hover over of physician name to show details for that physician. Click on name shows the details until the user clicks to hide.  Actually expand physician block to fill in a hidden div that shows the physician details but in the same block line with that Dr.   Then when hiding all we have to do is clear that div. 

* Allow additional filters to limit number of providers returned from the initial set.

* When Zip is successfully entered should populate City and State automatically.

* When Geopoint is known by client pass it to service instead of zipcode since that allows bypass of first 

  * When Geopoint is known by client pass it to service instead of zipcode since that allows bypass of first 

* When typing city name the center display should change to show the list of cities found along with list of zipcodes located.

  * When typing into city produce filter list of cities that include those that include the typed search string.  When state is selected then filter the city list to that state.   When city is selected populate dropdown list for zip to show zips in the city. 

* When typing into name auto populate suggestions  based on search of company and Dr Name. When entering dentist name use it to auto complete the name options 

  * When entering dentist name make it refresh search results as they type.
  * Must be able to detect form changes to Dentist Name and Specialty to trigger new search. 
  * Implement name pop over http://www.cssflow.com/snippets/search-dropdown http://www.cssflow.com/snippets/tag/popover

* When Geopoint is known by client pass it to service instead of zipcode since that allows bypass of first 

* Implement sample GUI clickable map with placeholder screen. Render a Map with dentists showing as places.

* When Zip plus 4 is entered must get a more precise geopoint for that location.   If we must fetch it from geo-coding API then we should save the results in separate table and text file. 

  ​

## ProvSearch

* Update provSearch so each major step is timed.  Add these to the took array and add this to the response array.  Also log these to a separate file.   Provide a utility method addTook(label) to update the took array and addTook(label, startTime) when start time is specified then use it against current time.  When not present take the time from the last element in took.
* Add support so when zip code lookup fails calls Google API to obtain Geo Point and saves new more precise data in location index.
* Add Specialty to filter provSearch. 
* Add Name search filter to provSearch.
* Add Max Number of providers to provSearch.
* Allow provSearch to return sort as distance from centroid,    Quality of Name Filter match, or randomized.
* Add GeoPoint to be specified instead of Zipcode.
* Add support so when more address components are supplied they are used to refine the GeoPoint position to be more precise than zip code.
* Add Python integration tests for provSearch. utility for basic integration testing to demonstrate success
* Update health check to call elastic search and confirm the provider and location indexes have been populated.
* Update API Docs to properly show Samples and results.
* Modify startup configuration to write logs directly rather than use pipe level redirect.
* Write Docker file to configure and run the elastic search and Search service on same virt.  Must write the elastic search data external to the docker image on the virt.
* Update request to ensure a requestId, sessionID from header is added to response and to took timing.

## Data Load

* Enhance data load geopoint lookup for providers to use zip plus 4 or full address information to provide geopoint for provider.   It currently looks up the 5 digit zip which is OK but causes zero distance to display in some screens. 
* In the create_loc_index rather than failing records which do not have zipcodes should query the government geocoding service to add those to the input data.  This mostly applies to those on military bases.

## Documentation

### Advanced Elastic Search Configuration

- TODO: Listening on a different port

- TODO: Storing data in a different directory

- TODO: Ensuring Elastic search does not attempt to join the local cluster as a node.   Modifying for a cluster of 1

- TODO: Modifying Maximum Memory Utilization

- TODO: Modifying write buffering

  ​

## GeoCoding

- TODO:  Geocoding Add Support to Query for City, State, Address combinations When missing form our location index should Call Google API and then save results in local location cache.
- TODO: Provide export functionality to save current Location geocoding index to allow reloading in other servers and retentions in source control. 
- Use the US Census.gov service to geocode all the Physicians Address records so we can use them as a open source alternative data set. https://geocoding.geo.census.gov/geocoder/Geocoding_Services_API.html  [sample 1](https://geocoding.geo.census.gov/geocoder/locations/onelineaddress?address=4600+Silver+Hill+Rd%2C+Suitland%2C+MD+20746&benchmark=9&format=json) [sample 2](https://geocoding.geo.census.gov/geocoder/locations/onelineaddress?address=5981+Lynwood+Center+RD%2C+Bainbridge%20Island%2C+WA+98110&benchmark=9&format=json)  [other geocoding services](https://geoservices.tamu.edu/Services/Geocode/OtherGeocoders/)   [tamu geocoding service](http://geoservices.tamu.edu/Services/Geocode/WebService/)  [tamu geocoding example](http://geoservices.tamu.edu/Services/Geocode/WebService/GeocoderWebServiceHttpNonParsed_V04_01.aspx?streetAddress=9355%20Burton%20Way&city=Beverly%20Hills&state=ca&zip=90210&apikey=demo&format=json&census=true&censusYear=2000|2010&notStore=false&version=4.01) [geocodeio online batch](https://geocod.io/upload/) [google geocoding](https://developers.google.com/maps/documentation/geocoding/start)  [Alternate database](https://physiciandownload.com/) 
- ​
- ​





