# Search Demo

A list of screen pictures taken from an operational provider search system.  Intended to provide a visual tour before performing a full installation.   

------



## Provider Search Basic by Zip

![prov-search-basic](provider-search-basic.jpg)

This data is supplied by the US government as [Medicare]( https://data.medicare.gov/Physician-Compare/Physician-Compare-National-Downloadable-File/mj5m-pzi6) service providers and includes about 2.2 million providers across the USA.

All Styling is supplied using standard CSS so the page can easily be styled to accommodate a wide range of look and feel.

------



## Provider Search with Zip & Name Filter

Filters the list of providers to only those that have  a first or last name starting with "MI" or where at least one of the words of the business name start with "MI"

![provider-search-name-filter.jpg](provider-search-name-filter.jpg)

------



## Provider search  Find a Zip from City Name

Displays a list of cities found with one of the words in their name starting with the search string.  In this instance "vega" was entered so it is showing Las Vegas.   Note that it displays all the states where  the name matches unless the state is selected which will cause it to filter out any cities not in that state.  

Clicking on one of the zipcodes shown will re-run the provider search centered on that zipcode.

![provider-search-city-list.jpg](provider-search-city-list.jpg)



------

> This system represents an example of provider search powered by Elastic search,  Node.js and our custom libraries and schema design.     ***If you have need of similar systems then please [contact us](http://BayesAnalytic.com/contact) for help***.   
>

