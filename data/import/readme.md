



- See: https://greatdata.com/zip-codes-lat-long for subscription to Zip + 4 Geocoding database $499 per year.   Also see: https://catalog.data.gov/dataset/zip-plus-4 

- ​

- The file:  [US-Zip-Codes-from-2013-Government Data.txt](US-Zip-Codes-from-2013-Government-Data) is sourced from [github](https://gist.github.com/erichurst/7882666)  It is processed by [create_loc_index.py](../../util/create_loc_index.py) to create a starting set of ip to lat, lon lookup records in elastic search.

- The file [zipcodes.tsv](zipcodes) is sourced from http://federalgovernmentzipcodes.us/  A more comprehensive version including alternate city names is also available at the same location.   This file was used because it makes it feasible to obtain city and state from the ZipCode.

- The file [us_postal_codes_2012.csv](us_postal_codes_2012.csv) is sourced from https://www.aggdata.com/node/86 it also maps Zip code to city and state but provides better case management for City names for better display.

- The file [physicians.tsv.zip](physicians.tsv.zip) is sourced from https://data.medicare.gov/Physician-Compare/Physician-Compare-National-Downloadable-File/mj5m-pzi6  This file will be used to provide a online search tool using only public data.  Main problem is that it supplies addresses but does not supply geopoints so the records would need to be geo-code.

- [Patient Care Survey by Hospital 264K records](https://data.medicare.gov/Hospital-Compare/Patient-survey-HCAHPS-Hospital/dgck-syfz)

- [Nursing home deficiencies](https://data.medicare.gov/Nursing-Home-Compare/Deficiencies/r5ix-sfxw)  334K records.

- [Value based purchasing hospital performance](https://data.medicare.gov/Hospital-Compare/Hospital-Value-Based-Purchasing-HVBP-Total-Perform/ypbt-wvdk)

- [Medicare spending by hospital by claim](https://data.medicare.gov/Hospital-Compare/Medicare-Hospital-Spending-by-Claim/nrth-mfg3)

- [Hospital related infections](https://data.medicare.gov/Hospital-Compare/Healthcare-Associated-Infections-Hospital/77hc-ibv8) 172.9K records

- [Nursing home facilities](https://data.medicare.gov/Nursing-Home-Compare/Provider-Info/4pq5-n9py)  15,662 records

- [Timely and effective Care by facility](https://data.medicare.gov/Hospital-Compare/Timely-and-Effective-Care-Hospital/yv7e-xc69) 104K records

- [Drug and Disease databases](https://www.pharmgkb.org/downloads/)

- [FDA drug labeling download](https://open.fda.gov/downloads/)

- [Drug event database files in JSON FDA](https://open.fda.gov/downloads/) Human Drug Downloads /drug/event

- [List of all FDA data files available for download](https://api.fda.gov/download.json)

  ​


