// Company Confidential 2017
// Return a list of cities that contain the search string specified.
// if a state is specified then it will be applied as a filter.
// if lat/lon and maxDistance are specified then it will be 
// applied as an additional filter.  If both maxDistance and
// state are supplied then it will require records to match
// both filters.   Returns a aa list of records 
// states -> containing Citings - containing Zipcodes. 

var BasicHTTPServer = require('./BasicHTTPServer');
var GenericHTTPHandler = require('./GenericHTTPHandler');

var http_get = require('./HTTPGet.js');
var url = require('url');
var StrUtil = require("./StrUtil.js");
var HTTPGet = http_get;
var lgr = require('./Logger');
var util = require('util');
var cll = lgr.cll;

// * * * * * * * * * * * * 
// * * * * * CLASS * * * *
// * * * * * * * * * * * * 
// zipgeo lookup geopoint from zipcode
// This version is done as a simple handler to demonstrate
// easy way of calling elastic search.  
function Handler(server, req, resp, extraPath, config)
{
   //console.log("HandlerStatus extraPath=" + extraPath);
   
  var self = new GenericHTTPHandler.GenericHTTPHandler(server,req, resp, extraPath,config);
     //  Calls our superclass handler and sets up its main variables.	   
  self.self = self;
    
  // Process main form submission from End user 
  // for the search page. 
  function handle() {


      function onCitySearchData(pobj, xparms) {
          var self = xparms.self
          var b = self.b
          // HTTP Response from Elastic Search Arrives Here
          //console.log("OnProvSearchData pobj=", pobj);
          //console.log("L50: pobj=", JSON.stringify(pobj));
          //console.log("L51: self=", self);
          //console.log("L52: xparms=", xparms);          
          //console.log("L53: xxx pobj=", pobj);
          if ((pobj.err) && (pobj.err.length > 0)) {
              self.writeHead(404, { 'Content-Type': 'text/plain' });
              var msg = "L48: onCitySearchData Error in call uri=" + xparms.uri + " err=" + pobj.err;
              if (lgr.LWarn()) {
                  lgr.warn(msg);
                  b.b(msg);
              }
          }
          else {
              try {
                  var pbody = pobj.parsedBody;
                  var took = pbody.took;
                  //console.log("L71 pbody=", util.inspect(pbody));                                    
                  //console.log("L62: parms=" + util.inspect(xparms));
                  //console.log("L76: source=", pbody._source);
                  if ((pbody !== undefined) && (pbody.hits !== undefined)) {
                      //console.log("L79: source=", pbody._source);
                      self.res.writeHead(200, { 'Content-Type': 'text/json' });
                      var locByKey = {}
                      var locByKeyAlreadInc = {};
                      var tout = { "hits": {}};

                      // Convert elastic search buckets into a domain
                      // friendly form. 
                      tout.in = xparms.in;
                      tout.hits.cout =  pbody.hits.total
                   
                      var statebucks = pbody.aggregations.state.buckets;
                      var states = [];
                      tout.states = states;
                      for (var statendx in statebucks) {
                          //console.log("pvdx ndx=", ndx);
                          var statebuck = statebucks[statendx];
                          var stateout = { "code": statebuck.key, "cities": [] };
                          var citiesout = stateout.cities;
                          states.push(stateout);
                          var cities = statebuck.city.buckets;
                          for (var cityndx in cities) {
                              var cityout = {};
                              var citybuck = cities[cityndx];
                              cityout.name = citybuck.key;
                              var zipsout = [];
                              cityout.zip = zipsout;
                              citiesout.push(cityout);
                              var zips = citybuck.zip.buckets;
                              for (var zipndx in zips) {
                                  var zipbuck = zips[zipndx];
                                  zipsout.push(zipbuck.key);
                              }
                          }
                      }
                      tout.took = took;
                      b.b(JSON.stringify(tout));

                  } else {
                      var msg = "L69: BAD DATA could not find hits =" + JSON.stringify(pobj);
                      self.writeHead(404, { 'Content-Type': 'text/plain' });
                      lgr.warn(msg);
                      b.b(msg);
                  }
                  //console.log("" + " elap=" + elap  " xparms.uri=" + xparms.uri);
              } catch (err) {
                  self.writeHead(404, { 'Content-Type': 'text/plain' });
                  msg = "L77: error parsing " + err + " " + err.message + " " + err.stack + " parse str=" + util.inspect(pobj, null, 2);
                  if (lgr.LWarn()) { lgr.warn("onProvSearchData", msg) };
                  b.b(msg);
              }
          }
          b.nl();
          self.flush();
          resp.end();
      } // onSearchData


      function runProvSearch(reqParms) {
          var self = reqParms.self;
          //console.log("L90: runProvSearch");
         
      } // runProvSearch


      // Main Search Handle searches Elastic Search for
      // geopoint centroid for the zipcode Then if the 
      // zipcode if located Searches for closeby search
      var hqueue = HTTPGet.GetAjaxQueue("elastic", 30, 1500);
      var b = self.b;
      self.req = req;
      self.resp = resp;
      var tin = {};
      tin.req = {}
      tin.req.parms = url.parse(self.req.url, true).query;
      console.log("L182: HandlerCitySearch parms=", tin.req.parms);
      tin.req.url = self.req.url;
      tin.req.extra_path = self.extraPath;
      tin.req.headers = self.req.headers;
      var reqParms = { 'in': tin };
      tin.maxDist = self.parseParmsFloat("maxDist", null);
      tin.lat = self.parseParmsFloat("lat", null);
      tin.lon = self.parseParmsFloat("lon", null);
      tin.city = self.parseParmsStr("city", null);
      tin.state = self.parseParmsStr("state", null);
      //console.log("L172: tin=", tin);
      //console.log("L196: reqParms.uri=", reqParms.uri);
      reqParms.server = self.server.elastic.host;
      reqParms.port = self.server.elastic.port;
      reqParms.qtype = "citySearch";
      reqParms.method = "POST";
      reqParms.self = self;
      reqParms.in = tin;
      reqParms.uri = "/location/loc/_search";
      reqParms.headers = { "ContentType": "application/json" };

      //lgr.log("L197: runSearch uri=", reqParms.uri, "server=", reqParms.server, reqParms.port);
     
      var tin = reqParms.in;
      var loc = tin.loc;
      var maxDist = tin.maxDist;
      //console.log("L99: maxDist=", maxDist, " loc=", loc);

      var query = {
          "bool": {
              "must": []
          }

      };

      var must = query.bool.must;

      if (tin.city !== null) {
          var cityarr = tin.city.split(" ");
          for (var tkndx in cityarr) {
              atoken = cityarr[tkndx].trim();
              if (atoken.length >= 2) {
                  must.push({ "match": { "city.edge": atoken } });
              }
          }
      }

      if (tin.state !== null) {
          must.push({ "term": { "state": tin.state.toUpperCase() } });
      }

      if ((tin.lat !== null) && (tin.lon !== null) && (tin.maxDist !== null)) {
          query.bool.filter = {
              "geo_distance": {
                  "distance": tin.maxDist + "mi",
                  "loc": {
                      "lat": tin.lat,
                      "lon": tin.lon
                  }
              }
          };
      }

      if ((must.length < 1) && (query.bool.filter === undefined)) {
          self.writeHead(400, { 'Content-Type': 'text/plain' });
          var msg = "L177: runCitySearch at least one of city, state, (lat,lon,maxDist) must be supplied parms=" + JSON.stringify(tin);
          b.b(msg);
          self.flush();
          resp.end();
          return;
      }
      if (must.length < 1) {
          must.push({ "match_all": {} });
      }


      var aggs = {
          "state": {
              "terms": {
                  "field": "state",
                  "order": { "_term": "asc" },
                  "size" : 100
              },
              "aggs": {
                  "city": {
                      "terms": {
                          "field": "city.raw",
                          "order": { "_term": "asc" },
                          "size": 50
                      },
                      "aggs": {
                          "zip": {
                              "terms": {
                                  "field": "zip",
                                  "order": { "_term": "asc" },
                                  "size" : 50
                              }
                          }
                      }
                  }
              }
          }
      };


      reqParms.body = {
          "size": 0,
          "query": query,
          "aggs" : aggs
      };

      reqParms.postStr = JSON.stringify(reqParms.body);
      //console.log("L234: Query =", JSON.stringify(reqParms.body, null, 2));
      hqueue.enqueue(reqParms, onCitySearchData);
      //console.log("L123: request queued")
  } // handle()
  
  self.handle = handle;
  return self;
} // Handler
exports.Handler = Handler;
