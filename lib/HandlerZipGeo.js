// Copyright (c) 2014 <Joseph Ellsworth, Bayes Analytic> - See use terms in License.txt
// Return the lat,lon of specified zip if found in elastic search index

var BasicHTTPServer = require('./BasicHTTPServer');
var GenericHTTPHandler = require('./GenericHTTPHandler');

var http_get = require('./HTTPGet.js');
var url = require('url');
var StrUtil = require("./StrUtil.js");
var HTTPGet = http_get;
var lgr = require('./Logger');
var util = require('util');
var cll = lgr.cll;

// * * * * * * * * * * * * 
// * * * * * CLASS * * * *
// * * * * * * * * * * * * 
// zipgeo lookup geopoint from zipcode
// This version is done as a simple handler to demonstrate
// easy way of calling elastic search.  
function Handler(server, req, resp, extraPath, config)
{
   //console.log("HandlerStatus extraPath=" + extraPath);
   
  var self = new GenericHTTPHandler.GenericHTTPHandler(server,req, resp, extraPath,config);
     //  Calls our superclass handler and sets up its main variables.	   
  self.self = self;
    
  // Process main form submission from End user 
  // for the search page. 
  function handle() {

      /* Receives a callback with the data or error
       from the request to the remote server.*/
      http://127.0.0.1:9839/zipgeo/14094
      function onSearchData(pobj, xparms) {
          var self = xparms.self
          var b = self.b
          // HTTP Response from Elastic Search Arrives Here
          //console.log("L50: pobj=", JSON.stringify(pobj));
          //console.log("L51: self=", self);
          //console.log("L52: xparms=", xparms);          
          //console.log("L53: xxx pobj=", pobj);
          if ((pobj.err) && (pobj.err.length > 0)) {
              self.writeHead(404, { 'Content-Type': 'text/plain' });
              var msg = "L60: onSearchData Error in call uri=" + xparms.uri + " err=" + pobj.err;
              if (lgr.LWarn()) {
                  lgr.warn(msg);
                  b.b(msg);
              }
          }
          else {
              try {
                  var pbody = pobj.parsedBody;
                  var took = pbody.took;
                  //console.log("L71 pbody=", pbody);                                    
                  //console.log("parms=" + util.inspect(xparms));
                  //console.log("L76: source=", pbody._source);
                  if ((pbody !== undefined) && (pbody.found === true) && (pbody._source !== undefined)) {
                      //console.log("L79: source=", pbody._source);
                      self.res.writeHead(200, { 'Content-Type': 'text/json' });
                      var source = pbody._source
                      b.b(JSON.stringify(pobj.parsedBody._source.loc));
                  } else {
                      var msg = "L76: BAD DATA could not find loc =" + JSON.stringify(pobj);
                      self.writeHead(404, { 'Content-Type': 'text/plain' });
                      lgr.warn(msg);
                      b.b(msg);
                  }
                  //console.log("" + " elap=" + elap  " xparms.uri=" + xparms.uri);
              } catch (err) {
                  self.writeHead(404, { 'Content-Type': 'text/plain' });
                  msg = "L68: error parsing " + err + " " + err.message + " " + err.stack + " parse str=" + util.inspect(pobj, null, 2);
                  if (lgr.LWarn()) { lgr.warn("HandlerZipGeo", msg) };
                  b.b(msg);
              }
          }
          b.nl();
          self.flush();
          resp.end();
      } // onSearchData

      // Main Handle
      var hqueue = HTTPGet.GetAjaxQueue("elastic", 30, 1500);
      var b = self.b;
      self.req = req;
      self.resp = resp;
      var tin = {};
      tin.req = {}
      tin.req.parms = url.parse(self.req.url, true).query;
      tin.req.url = self.req.url;
      tin.req.extra_path = self.extraPath;
      tin.req.headers = self.req.headers;
      //console.log("server=", self.server);
      //console.log("elastic=", self.server.elastic)
      // http://localhost:9200/location/loc/14094 
      //console.log("runSearch ");
      //console.log("L91: self=", self);
      //console.log("L92: server=", self.server);
      var reqParms = { 'in': tin };
      reqParms.uri = "/location/loc/" + self.extraPath;
      reqParms.server = self.server.elastic.host;
      reqParms.port = self.server.elastic.port;
      reqParms.qtype = "zipgeo";
      reqParms.method = "GET";
      reqParms.header = "";
      reqParms.self = self;
      reqParms.in = tin;
      lgr.log("L108: runSearch uri=", reqParms.uri, "server=", reqParms.server, reqParms.port);
      reqParms.req_start_time = StrUtil.curr_time();
      hqueue.enqueue(reqParms, onSearchData);
  } // handle()
  
  self.handle = handle;
  return self;
} // Handler
exports.Handler = Handler;
