// Company Confidential 2017
// Return a list of providers close to the specified zip code
// or specified address if supplied.   If the address is specified 
// but not not found then use Google geo-coding to retrive a lat/lon
// and cache that location. 

var BasicHTTPServer = require('./BasicHTTPServer');
var GenericHTTPHandler = require('./GenericHTTPHandler');

var http_get = require('./HTTPGet.js');
var url = require('url');
var StrUtil = require("./StrUtil.js");
var HTTPGet = http_get;
var lgr = require('./Logger');
var util = require('util');
var cll = lgr.cll;

// * * * * * * * * * * * * 
// * * * * * CLASS * * * *
// * * * * * * * * * * * * 
// zipgeo lookup geopoint from zipcode
// This version is done as a simple handler to demonstrate
// easy way of calling elastic search.  
function Handler(server, req, resp, extraPath, config)
{
   //console.log("HandlerStatus extraPath=" + extraPath);
   
  var self = new GenericHTTPHandler.GenericHTTPHandler(server,req, resp, extraPath,config);
     //  Calls our superclass handler and sets up its main variables.	   
  self.self = self;
    
  // Process main form submission from End user 
  // for the search page. 
  function handle() {


      function onProvSearchData(pobj, xparms) {
          var self = xparms.self
          var b = self.b
          // HTTP Response from Elastic Search Arrives Here
          //console.log("OnProvSearchData pobj=", pobj);
          //console.log("L50: pobj=", JSON.stringify(pobj));
          //console.log("L51: self=", self);
          //console.log("L52: xparms=", xparms);          
          //console.log("L53: xxx pobj=", pobj);
          if ((pobj.err) && (pobj.err.length > 0)) {
              self.writeHead(404, { 'Content-Type': 'text/plain' });
              var msg = "L48: onProvSearchData Error in call uri=" + xparms.uri + " err=" + pobj.err;
              if (lgr.LWarn()) {
                  lgr.warn(msg);
                  b.b(msg);
              }
          }
          else {
              try {
                  var pbody = pobj.parsedBody;
                  var took = pbody.took;
                  //console.log("L71 pbody=", pbody);                                    
                  //console.log("parms=" + util.inspect(xparms));
                  //console.log("L76: source=", pbody._source);
                  if ((pbody !== undefined) && (pbody.hits !== undefined) && (pbody.hits.total > 0)) {
                      //console.log("L79: source=", pbody.pobj);
                      self.res.writeHead(200, { 'Content-Type': 'text/json' });
                      var locByKey = {}
                      var locByKeyAlreadInc = {};
                      var tout = {};
                      var locs = [];
                      tout.locations = locs;
                      tout.in = xparms.in;
                      var tin = xparms.in;
                      hits = pbody.hits.hits
                      //console.log("hits=", hits);
                      for (var ndx in hits) {
                          //console.log("pvdx ndx=", ndx);
                          var ahit = hits[ndx];
                          //console.log("ahit=", ahit);
                          var asrc = ahit._source;
                          asrc._id = ahit._id;
                          //console.log("asrc=", asrc);
                          var dist = ahit.fields.distance;
                          var combName = asrc.drName.first + " " + asrc.drName.middle + " " + asrc.drName.last;
                          asrc.distance = dist * 0.000621371; // convert meter to miles
                          // ------
                          // Transform the Set of Providers as a flat list into 
                          // a distinct set of Locations each containing 
                          // a provider.  The key is a combination of business name
                          // and address.
                          // ------
                          var ukey = asrc.uniqueLocKey;
                          var aloc = locByKey[ukey];
                          if (aloc === undefined) {
                              aloc = {}
                              aloc.orgName = asrc.orgName;                              
                              aloc.addr = asrc.addr;
                              aloc.distance = asrc.distance;
                              aloc.loc = asrc.loc;
                              aloc.phone = asrc.phone;
                              aloc.providers = [];
                              locByKey[ukey] = aloc;
                              locs.push(aloc);
                              locByKeyAlreadInc[ukey] = [combName]
                          }
                          
                          //console.log("L76:");
                          delete asrc.orgName;
                          delete asrc.addr;
                          //delete asrc.uniqueLocKey;
                          delete asrc.loc;
                          delete asrc.distance;
                          var pvds = aloc.providers;
                          if (!(combName in locByKeyAlreadInc[ukey])) {
                              // Only include this provider if they are 
                              // not already in the data set.
                              pvds.push(asrc);
                              locByKeyAlreadInc[ukey][combName] = true;
                          }
                      }
                      if (("specialty" in tin.views) && (pbody.aggregations) && (pbody.aggregations.specialty)) {
                          var specsin = pbody.aggregations.specialty.buckets;
                          specsout = [];
                          for (var bndx in specsin) {
                              aspec = specsin[bndx];
                              aspout = { "name": aspec.key, "count": aspec.doc_count };
                              specsout.push(aspout);
                          }
                          tout.specialty = specsout;
                      }
                      tout.took = took;
                      b.b(JSON.stringify(tout));

                  } else {
                      var msg = "L69: BAD DATA could not find loc =" + JSON.stringify(pobj);
                      self.writeHead(404, { 'Content-Type': 'text/plain' });
                      lgr.warn(msg);
                      b.b(msg);
                  }
                  //console.log("" + " elap=" + elap  " xparms.uri=" + xparms.uri);
              } catch (err) {
                  self.writeHead(404, { 'Content-Type': 'text/plain' });
                  msg = "L77: error parsing " + err + " " + err.message + " " + err.stack + " parse str=" + util.inspect(pobj, null, 2);
                  if (lgr.LWarn()) { lgr.warn("onProvSearchData", msg) };
                  b.b(msg);
              }
          }
          b.nl();
          self.flush();
          resp.end();
      } // onSearchData


      function runProvSearch(reqParms) {
          var self = reqParms.self;
          //console.log("L90: runProvSearch");
          reqParms.uri = "/prov/prov/_search";
          reqParms.qtype = "provsearch";
          reqParms.method = "POST";
          reqParms.headers = { "ContentType": "application/json" };
          
          var tin = reqParms.in;
          var loc = tin.loc;
          var maxDist = tin.maxDist;
          //console.log("L99: maxDist=", maxDist, " loc=", loc);
          reqParms.body = {
              "size" : 300,
              "query": {
                  "bool": {
                      "must": [
                   
                      ],
                      "filter": {
                          "geo_distance": {
                              "distance": maxDist + "mi",
                              "loc": {
                                  "lat": loc.lat,
                                  "lon": loc.lon
                              }
                          }
                      }
                  }
              },
              "script_fields": {
                  "distance": {
                      "script": "doc['loc'].arcDistance(" + loc.lat + "," + loc.lon + ")"
                  }                 
              },
              "stored_fields": ["_source"]
          }

          var aggs = {
              "specialty": {
                  "terms": {
                      "field": "specialty.raw",
                      "order": { "_term": "asc" },
                      "size": 250
                  }
              }
          };

          if ("specialty" in tin.views) {
              reqParms.body.aggs = aggs;
          }

          must = reqParms.body.query.bool.must;
          //console.log("L178: query=", reqParms.body.query, "\n\tTIN=", tin, "\n\tmust=", must);
          if (tin.name !== null) {
              var tkarr = tin.name.split(" ");
              for (var tkndx in tkarr) {
                  var atoken = tkarr[tkndx].trim();
                  if (atoken.length >= 2) {
                      must.push({ "match": { "names.edge": atoken } });
                  }
              }
          }
          //console.log("L188: query=", reqParms.body.query);

          if (tin.specialty != null) {
              var specphrase = {
                  "match": {
                      "specialty": {
                          "query": tin.specialty,
                          "operator": "and"
                      }
                  }
              };
              must.push(specphrase);
          }

          if (must.length < 1) {
              must.push({ "match_all": {} });
          }
          //console.log("L193: query=", reqParms.body.query);
              /*,"aggs": {
              "locations": {
                  "terms": { "field": "uniqueLocKey" },
                  "aggs": {
                      "byukey": { "top_hits": { "size": 120 } } } } } */

          // See Also https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-scripting-fields.html to sort by distance script

          //console.log("200 reqParms.body = ", reqParms.body);
          reqParms.postStr = JSON.stringify(reqParms.body);
          //console.log("L120: conversion of request to str ok");
          //console.log("L121: runProvSearch postStr=", reqParms.postStr);
          hqueue.enqueue(reqParms, onProvSearchData);
          //console.log("L123: request queued")
      } // runProvSearch



      /* Receives a callback with the data or error
       from the request to the remote server.*/
      http://127.0.0.1:9839/zipgeo/14094
      function onZipcodeSearchData(pobj, xparms) {
          var self = xparms.self
          var b = self.b
          // HTTP Response from Elastic Search Arrives Here
          //console.log("L50: pobj=", JSON.stringify(pobj));
          //console.log("L51: self=", self);
          //console.log("L52: xparms=", xparms);          
          //console.log("L53: xxx pobj=", pobj);
          if ((pobj.err) && (pobj.err.length > 0)) {
              self.writeHead(404, { 'Content-Type': 'text/plain' });
              var msg = "L137: onSearchData Error in call uri=" + xparms.uri + " err=" + pobj.err;
              if (lgr.LWarn()) {
                  lgr.warn(msg);                  
              }
              b.b("failed to load zipcode\n");
              b.b(msg);
              self.flush();
              resp.end();
              return;
          }
          else {
              try {
                  var pbody = pobj.parsedBody;
                  var took = pbody.took;
                  //console.log("L71 pbody=", pbody);                                    
                  //console.log("parms=" + util.inspect(xparms));
                  //console.log("L76: source=", pbody._source);
                  if ((pbody !== undefined) && (pbody.found === true) && (pbody._source !== undefined)) {
                      //console.log("L79: source=", pbody._source);
                      var source = pbody._source         
                      source._id = 
                      xparms.loc = pobj.parsedBody._source.loc;
                      xparms.in.loc = xparms.loc;
                      runProvSearch(xparms);
                  } else {
                      var msg = "L157: BAD DATA could not find loc =" + JSON.stringify(pobj);
                      self.writeHead(404, { 'Content-Type': 'text/plain' });
                      lgr.warn(msg);
                      b.b(msg);
                  }
                  //console.log("" + " elap=" + elap  " xparms.uri=" + xparms.uri);
              } catch (err) {
                  self.writeHead(404, { 'Content-Type': 'text/plain' });
                  msg = "L165: error parsing " + err + " " + err.message + " " + err.stack + " parse str=" + util.inspect(pobj, null, 2);
                  if (lgr.LWarn()) { lgr.warn("HandlerZipGeo", msg) };
                  b.b(msg);
              }
          }         
      } // onSearchData

      // Main Search Handle searches Elastic Search for
      // geopoint centroid for the zipcode Then if the 
      // zipcode if located Searches for closeby search
      var hqueue = HTTPGet.GetAjaxQueue("elastic", 30, 1500);
      var b = self.b;
      self.req = req;
      self.resp = resp;
      var tin = {};
      tin.req = {}
      tin.req.parms = url.parse(self.req.url, true).query;
      //console.log("L182: parms=", tin.req.parms);
      tin.req.url = self.req.url;
      tin.req.extra_path = self.extraPath;
      tin.req.headers = self.req.headers;
      var reqParms = { 'in': tin };
      if (self.parms.zipcode === undefined) {
          var msg = "L189: zipcode is a mandatory parameter";
          self.writeHead(300, { "Content-type": "text/plain" });
          b.b(msg);
          //console.log(msg);
          return;
      }
      tin.zipcode = self.parms.zipcode;
      tin.maxDist = self.parseParmsFloat("maxDist", 20);
      tin.name = self.parseParmsStr("name", null);
      tin.views = self.parseParmsStrAsDict("v", {});
      tin.specialty = self.parseParmsStr("specialty", null);
      //console.log("L303 tin.views=", tin.views)
      
      reqParms.uri = "/location/loc/" + tin.zipcode;
      //console.log("L196: reqParms.uri=", reqParms.uri);
      reqParms.server = self.server.elastic.host;
      reqParms.port = self.server.elastic.port;
      reqParms.qtype = "ProvSearch";
      reqParms.method = "GET";
      reqParms.header = "";
      reqParms.self = self;
      reqParms.in = tin;
      //lgr.log("L197: runSearch uri=", reqParms.uri, "server=", reqParms.server, reqParms.port);
      reqParms.req_start_time = StrUtil.curr_time();
      hqueue.enqueue(reqParms, onZipcodeSearchData);
  } // handle()
  
  self.handle = handle;
  return self;
} // Handler
exports.Handler = Handler;
